import os
#import getpass


json_file = [
	## basic version of centernet:
    #3051 3052 3062
    # change number of gpus
    # './centernet_loss/json/testori.json',
    # './centernet_loss/json/testori2.json',
    # './centernet_loss/json/testori4.json',
    # 0.69xx
    # 2gpu 4gpu 1gpu: 3:38h 4:44h 5:54h
    # conclusion: 2gpu is faster than 1gpu and 4gpu, maybe should adjust the learning rate

    ## change learning rate, when use the dense_wh(for further exp)
    #3094 3095 3096
    # './centernet_loss/json/testoridwh2.json',
    # './centernet_loss/json/testoridwh2lrd2.json',
    # './centernet_loss/json/testoridwh2lrx2.json',
    #conclusion: 
    # well, dense_wh make the net convergence slower?
    # Guess, this is because the net first learned the one aspect then another?
    # or, just not work well (the defult setting should be the best one)

    ## comments the ambiguity part in loss map:

    # a comparation with the fc loss and the basic loss
    # 3478/3481 3479
    # './centernet_loss/json/bs.json', # 3821/3876
    # './centernet_loss/json/fc.json', # 6957

    # adjust the scale factor
    # 3483 3484 3485 3486  
    # './centernet_loss/json/bs.json', #1p00 #36
    # './centernet_loss/json/bs0p25.json', # 6859
    # './centernet_loss/json/bs0p33.json', # 6829
    # './centernet_loss/json/bs0p50.json', # 6030

    #3496 3497 3498 3499
    # './centernet_loss/json/bs0p67.json', #53
    # './centernet_loss/json/bs0p75.json', #51
    # './centernet_loss/json/bs0p90.json', #43
    # './centernet_loss/json/bs0p99.json', #41 

    # 3624 3625 3626
    # './centernet_loss/json/bs0p05.json', 
    # './centernet_loss/json/bs0p10.json', 
    # './centernet_loss/json/bs0p15.json', 

    # have to first visualize the results, to show that smooth heatmap is important
    


    # quasi-smooth centernet:

]

usrname = 'v-yuph'
passwd = 'The1stday.'


def main():
    for f in json_file:
        cmd = 'curl -k --ntlm --user ' + usrname + ':'+ passwd +\
        ' -X POST -H "Content-Type: application/json" --data @' + f  + \
        ' https://philly/api/v2/submit'
        os.system(cmd)


if __name__ == '__main__':
    main()
