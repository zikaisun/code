import numpy as np
import cv2


def posedf_to_flow(_pd, weight, height, width):
    pd = _pd * weight

    n, h, w = pd.shape
    assert height / h == width / w

    pd_x = pd[0:n:3, :, :] * width
    pd_y = pd[1:n:3, :, :] * height

    pd_x = np.sum(pd_x, axis=0)
    pd_y = np.sum(pd_y, axis=0)

    pd_x = cv2.resize(pd_x, (height, width), interpolation=cv2.INTER_NEAREST)
    pd_y = cv2.resize(pd_y, (height, width), interpolation=cv2.INTER_NEAREST)

    flow = np.zeros((height, width, 2), dtype=np.float32)
    flow[:, :, 0] = pd_x
    flow[:, :, 1] = pd_y

    return flow


def flow_to_posedf(flow, pd):
    n, pd_height, pd_width = pd.shape
    flow_height, flow_width, _ = flow.shape

    flow_x = flow[:, :, 0] / flow_width
    flow_y = flow[:, :, 1] / flow_height

    flow_x = cv2.resize(flow_x, (pd_height, pd_width), interpolation=cv2.INTER_NEAREST)
    flow_y = cv2.resize(flow_y, (pd_height, pd_width), interpolation=cv2.INTER_NEAREST)

    flow_pd = pd.copy()

    flow_pd[0:n:3, :, :] = flow_x
    flow_pd[1:n:3, :, :] = flow_y

    return flow_pd


def flow_to_joint_flow(flow, weight):
    height, width, _ = flow.shape
    n, h, w = weight.shape
    weight = weight[0:n:3, :, :]
    flow_joint = np.zeros((height, width, 2), dtype=np.float32)
    for n_jt in range(0, int(n / 3)):
        wt = weight[n_jt]
        wt = cv2.resize(wt, (height, width), interpolation=cv2.INTER_NEAREST)
        flow_joint[:, :, 0] = flow_joint[:, :, 0] + wt * flow[:, :, 0]
        flow_joint[:, :, 1] = flow_joint[:, :, 1] + wt * flow[:, :, 1]
    return flow_joint
