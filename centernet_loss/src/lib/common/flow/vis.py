import numpy as np
import cv2


def flow_visualize_v2(data2d):
    h, w, _ = data2d.shape
    hsv = np.zeros((h, w, 3), dtype=np.uint8)
    hsv[..., 1] = 255
    mag, ang = cv2.cartToPolar(data2d[..., 0], data2d[..., 1])
    hsv[..., 0] = ang * 180 / np.pi / 2
    hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
    # plt.imshow(bgr)
    # plt.show()
    # plt.imshow(data2d[..., 0], cmap='hot', interpolation='nearest')
    # plt.colorbar()
    # plt.show()
    # plt.imshow(data2d[..., 1], cmap='hot', interpolation='nearest')
    # plt.colorbar()
    # plt.show()
    return bgr


def flow_color_map_vis_v2(height, width):
    size_y = height
    size_x = width
    map = np.zeros((size_y, size_x, 2))
    for y in range(0, size_y):
        for x in range(0, size_x):
            map[y, x, 0] = x - size_x / 2.0
            map[y, x, 1] = y - size_y / 2.0
    bgr = flow_visualize_v2(map)
    return bgr
