import numpy as np
import pickle


def readFlo_v2(filename, normalize=False):
    """
    Read from Middlebury .flo file
    :param flow_file: name of the flow file
    :return: optical flow data in matrix
    """
    f = open(filename, 'rb')
    magic = np.fromfile(f, np.float32, count=1)
    data2d = None

    if 202021.25 != magic:
        print('Magic number incorrect. Invalid .flo file')
    else:
        w = np.fromfile(f, np.int32, count=1)
        h = np.fromfile(f, np.int32, count=1)
        # print "Reading %d x %d flow file in .flo format" % (h, w)
        data2d = np.fromfile(f, np.float32, count=int(2 * w * h))
        if not np.isfinite(data2d).all():
            errf = open("error_flo.txt", 'a')
            errf.write(filename + '\n')
            errf.close()
            data2d[:] = 0
        # reshape data into 3D array (columns, rows, channels)
        data2d = np.resize(data2d, (h[0], w[0], 2))
    f.close()

    # data2d = cv2.resize(data2d, None, None, fx=2, fy=2, interpolation=interpolation) * 2
    # data2d = cv2.resize(data2d, dst=None, dsize=flow_size, fx=0, fy=0, interpolation=cv2.INTER_LINEAR)
    # data2d[:, :, 0] = data2d[:, :, 0] * 1.0 * flow_size[0] / w[0]
    # data2d[:, :, 1] = data2d[:, :, 1] * 1.0 * flow_size[1] / h[0]

    # h_paddng = (flow_size[1] - h[0])//2
    # w_padding = (flow_size[0] - w[0])//2
    # data2d = np.pad(data2d, ((h_paddng, h_paddng), (w_padding, w_padding), (0, 0)), mode=padding)

    if normalize:
        data2d = (data2d - np.mean(data2d, axis=(0, 1))) / np.std(data2d, axis=(0, 1))
        # data2d = data2d / np.max(np.abs(data2d), axis=(0,1))

    # data2d_tensor = np.zeros((1, 2, data2d.shape[0], data2d.shape[1]))
    # for i in range(2):
    #     data2d_tensor[0, i, :, :] = data2d[:, :, i]

    # return data2d_tensor
    return data2d


def writeFlow(name, flow):
    f = open(name, 'wb')
    f.write('PIEH'.encode('utf-8'))
    np.array([flow.shape[1], flow.shape[0]], dtype=np.int32).tofile(f)
    flow = flow.astype(np.float32)
    flow.tofile(f)


def writeFlowPickle(name, flow):
    with open(name, 'wb') as fid:
        pickle.dump(flow, fid, pickle.HIGHEST_PROTOCOL)
    # print("Writing flow to", name)


def readFlowPickle(name):
    with open(name, 'rb') as fid:
        flow = pickle.load(fid)
    return flow
