import sys
import numpy as np


def gen_jr(num, alpha, step):
    jr = np.zeros((num * 2, num), dtype=np.float)
    for i in range(0, 2 * num):
        for j in range(0, num):
            if (0 <= i < num) and i == j:
                jr[i, j] = 1
            elif (num <= i < 2 * num) and (i - j) == num:
                jr[i, j] = alpha
            elif (num <= i < 2 * num) and (i - j) == (num + step):
                jr[i, j] = -alpha
            else:
                jr[i, j] = 0
    return jr


def gauss_newton_core(pm, dpm, num, step, alpha, iter_num, jr=None):
    """
    https://en.wikipedia.org/wiki/Gauss%E2%80%93Newton_algorithm
    :param pm:
    :param dpm:
    :param num:
    :param step:
    :param alpha:
    :param iter_num:
    :param jr:
    :return:
    """
    if jr is None:
        jr = gen_jr(num, alpha, step)

    # 1. init b0 with pm
    bm = pm.copy()
    for n_iter in range(0, iter_num):
        # 2. iteration
        rm = np.zeros((num * 2, 1), dtype=np.float)
        # 3. current rm
        rm[0:num] = bm - pm
        bm_ = bm.copy()
        if step > 0:
            bm_[step:num] = bm[0:num - step].copy()
        else:
            bm_[0:num + step] = bm[-step:num].copy()
        rm[num:2 * num] = alpha * (bm - bm_ - dpm)
        # 4. update bm
        bm = bm - np.matmul(np.linalg.pinv(jr), rm)
    return bm, jr


def optimize_videos(video_start_frame_ids, single_img_res, tracking_img_delta, alpha, iter_num, step):
    """
    :param video_start_frame_ids:
    :param single_img_res:
    :param tracking_img_delta:
    :param alpha:
    :param iter_num:
    :param step:
    :return:
    """
    optimized_img_res = single_img_res.copy()
    dim_num = single_img_res.shape[1]
    video_num = len(video_start_frame_ids) - 1
    for n_video in range(0, video_num):
        sys.stdout.write("\r{0}%".format(100 * n_video / video_num))
        sys.stdout.flush()
        start_id, end_id = video_start_frame_ids[n_video], video_start_frame_ids[n_video + 1]
        num = end_id - start_id
        jr = None
        for n_dim in range(0, dim_num):
            pm = single_img_res[start_id:end_id, n_dim]
            dpm = tracking_img_delta[start_id:end_id, n_dim]
            bm, jr = gauss_newton_core(pm, dpm, num, step, alpha, iter_num, jr)
            optimized_img_res[start_id:end_id, n_dim] = bm.copy()
    return optimized_img_res
